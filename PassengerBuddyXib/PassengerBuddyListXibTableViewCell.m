//
//  PassengerBuddyListXibTableViewCell.m
//  carpoolarabia_ios
//
//  Created by Mac on 11/20/15.
//  Copyright © 2015 gritsay. All rights reserved.
//

#import "PassengerBuddyListXibTableViewCell.h"
#import "UserRatingView.h"
#import "CPALogger.h"

@interface PassengerBuddyListXibTableViewCell ()
@property (strong, nonatomic) IBOutlet UserRatingView *userRatingView;
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UIImageView *notificationView;
@property (strong, nonatomic) IBOutlet UILabel *notificationCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstName;
@property (strong, nonatomic) IBOutlet UIImageView *home;
@property (strong, nonatomic) IBOutlet UIImageView *work;
@property (strong, nonatomic) IBOutlet UIImageView *oneDirection;
@property (strong, nonatomic) IBOutlet UIImageView *bothDirections;
@property (strong, nonatomic) IBOutlet UIImageView *favoriteImage;
@property (strong, nonatomic) IBOutlet UIImageView *passengerNewView;
@end

@implementation PassengerBuddyListXibTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [self.avatar.layer setCornerRadius:CGRectGetHeight(self.avatar.frame)/2];
    [self.avatar.layer setMasksToBounds:YES];
    
    [self.notificationView.layer setCornerRadius:CGRectGetHeight(self.notificationView.frame)/2];
    [self.notificationView.layer setMasksToBounds:YES];
    
    [self svgImages];
}

- (void) addBothDirectionsToSuperView {
    [self.bothDirections setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.mainView addSubview:self.bothDirections];
    NSDictionary *views = @{@"both" : self.bothDirections, @"one" : self.oneDirection, @"work" : self.work, @"home" : self.home};
    [self.mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[home]-8-[both]-8-[work]" options:0 metrics:nil views:views]];
    [self.mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-26-[both]-1-[one]" options:0 metrics:nil views:views]];
}

- (void) drawingRidesWithToHome:(BOOL)toHome andToWork:(BOOL)toWork{
    [self.userRatingView setHidden:NO];
    [self hideAllDirections:!(toHome|toWork)];
    if(toHome && toWork){
        [self addBothDirectionsToSuperView];
    } else if (!toHome && !toWork) {
        [self.passengerNewView setHidden:YES];
        [self.favoriteImage setHidden:YES];
        [self.userRatingView setHidden:YES];
    } else if(toHome && !toWork){
        [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridehome_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
        self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.work setTintColor:RGBColor(0xdedede, 1)];
        [self.home setTintColor:RGBColor(0x28DA93, 1)];
        [self.bothDirections removeFromSuperview];
    } else {
        [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridework_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
        self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.home setTintColor:RGBColor(0xdedede, 1)];
        [self.work setTintColor:RGBColor(0x28DA93, 1)];
        [self.bothDirections removeFromSuperview];
    }
}

- (void)svgImages {
    SVGKImage *avatarImage = [SVGKImage imageNamed:@"i_portrait_grey"];
    [avatarImage setSize:self.avatar.frame.size];
    [self.avatar setImage:avatarImage.UIImage];
    
    [self.home setImage:[UIImage imageWithSVGNamed:@"i_home_green" targetSize:self.home.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.work setImage:[UIImage imageWithSVGNamed:@"i_work_green" targetSize:self.work.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridework_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.bothDirections setImage:[UIImage imageWithSVGNamed:@"i_ridehome_green" targetSize:self.bothDirections.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.arrowImage setImage:[UIImage imageWithSVGNamed:@"i_ios_rightbtn" targetSize:CGSizeMake(8, 16) fillColor:RGBColor(0x67717D, 1)]];
    
    SVGKImage *heartImage = [SVGKImage imageNamed:@"i_buddylist_heart"];
    [heartImage setSize:self.favoriteImage.frame.size];
    [self.favoriteImage setImage:heartImage.UIImage];
    
    SVGKImage *newImage = [SVGKImage imageNamed:@"i_newbtn"];
    [newImage setSize:self.passengerNewView.frame.size];
    [self.passengerNewView setImage:newImage.UIImage];
}

- (void)drawingRides {
    [self drawingRidesWithToHome:_toHome andToWork:_toWork];
}

- (void) hideAllDirections:(BOOL)hide {
    [self.home setHidden:hide];
    [self.work setHidden:hide];
    [self.oneDirection setHidden:hide];
    [self.bothDirections setHidden:hide];
}

- (void) executeCellSettings {
    if (self.favorite && self.passengerNew) {
        [self.favoriteImage setHidden:_favorite];
        [self.passengerNewView setHidden:!_passengerNew];
    } else {
        [self.favoriteImage setHidden:!_favorite];
        [self.passengerNewView setHidden:!_passengerNew];
    }
    
    [self drawingRides];
    [self checkTurnOff];
    
    if (_turn) {
        [self.avatar sd_setImageWithURL:_avatarURL placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:self.avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
    }
}

- (void) checkTurnOff {
    if (!self.turn) {
        [self changeViewColorsIsTurnOn:self.turn];
        [self.userRatingView progressLayerColorDisable:!self.turn];
        [self.avatar sd_setImageWithURL:_avatarURL placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:self.avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
        [self.avatar setAlpha:0.5f];
        [self.firstName setTextColor:RGBColor(0xe9e9e9, 1)];
        [self.passengerNewView setHidden:!_turn];
        [self.notificationView setHidden:!_turn];
        [self.favoriteImage setHidden:!_turn];
    } else {
        [self.avatar setAlpha:1.0f];
        [self.firstName setTextColor:RGBColor(0x575757, 1)];
        [self changeViewColorsIsTurnOn:self.turn];
        [self.userRatingView progressLayerColorDisable:NO];
        [self.userRatingView setRating:_rating];
    }
}

- (void)changeViewColorsIsTurnOn:(BOOL)isTurnOn {
    UIColor *color = isTurnOn ? RGBColor(0x28DA93, 1) : RGBColor(0xe9e9e9, 1);
    UIColor *arrowColor = isTurnOn ? RGBColor(0x67717D, 1) : RGBColor(0xe9e9e9, 1);
    
    self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.oneDirection.image = [self.oneDirection.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.bothDirections.image = [self.bothDirections.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.arrowImage.image = [self.arrowImage.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
   
    [self.home setTintColor:color];
    [self.work setTintColor:color];
    
    if((_toHome && !_toWork) && isTurnOn){
        [self.work setTintColor:RGBColor(0xe9e9e9, 1)];
        [self.home setTintColor:RGBColor(0x28DA93, 1)];
    } else if ((!_toHome && _toWork) && isTurnOn){
        [self.home setTintColor:RGBColor(0xe9e9e9, 1)];
        [self.work setTintColor:RGBColor(0x28DA93, 1)];
    } else if (!_toHome && !_toWork){
        [self.avatar setAlpha:0.5];
        [self.firstName setTextColor:RGBColor(0xe9e9e9, 1)];
    }

    [self.oneDirection setTintColor:color];
    [self.bothDirections setTintColor:color];
    [self.arrowImage setTintColor:arrowColor];
}

- (void)setName:(NSString *)name {
    [self.firstName setText:name];
}

- (void)setNotificationCount:(int)notificationCount {
    if (notificationCount == 0) {
        [self.notificationView setHidden:YES];
        [self.notificationCountLabel setHidden:YES];
    } else {
        [self.notificationView setHidden:NO];
        [self.notificationCountLabel setHidden:NO];
        [self.notificationCountLabel setText:[NSString stringWithFormat:@"%d", notificationCount]];
    }
}

- (void)setAvatarURL:(NSURL *)avatarURL {
    _avatarURL = avatarURL;
    [self.avatar sd_setImageWithURL:avatarURL placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:self.avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
}

- (void)setRating:(float)rating {
    _rating = rating;
    [self.userRatingView setRating:rating];
}

- (UIImage *)blackAndWhiteImage:(UIImage *)image
{
    CIImage *beginImage = [CIImage imageWithCGImage:image.CGImage];
    
    CIImage *output = [CIFilter filterWithName:@"CIPhotoEffectTonal" keysAndValues:kCIInputImageKey, beginImage, nil].outputImage;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiimage = [context createCGImage:output fromRect:output.extent];
    UIImage *newImage = [UIImage imageWithCGImage:cgiimage];
    
    CGImageRelease(cgiimage);
    
    return newImage;
}

@end