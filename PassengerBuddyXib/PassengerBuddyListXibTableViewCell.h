//
//  PassengerBuddyListXibTableViewCell.h
//  carpoolarabia_ios
//
//  Created by Mac on 11/20/15.
//  Copyright © 2015 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>

@interface PassengerBuddyListXibTableViewCell : SWTableViewCell
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImage;
@property (assign, nonatomic) BOOL toHome;
@property (assign, nonatomic) BOOL toWork;
@property (nonatomic, assign) BOOL favorite;
@property (nonatomic, assign) BOOL passengerNew;
@property (nonatomic, assign) BOOL turn;
@property (assign, nonatomic) int notificationCount;
@property (assign, nonatomic) float rating;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL *avatarURL;

@property (nonatomic, strong) Buddy *userData;
@property (nonatomic, strong) GetUserInfoInputModel *userInfo;

- (void) executeCellSettings;
@end
