//
//  DriverPassengerViaXibViewController.m
//  carpoolarabia_ios
//
//  Created by Admin on 19/02/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "DriverPassengerViaXibViewController.h"
#import "PersonalInformationXib.h"
#import "RideToBuddy.h"
#import "DriverInformation.h"
#import "AboutMeInformation.h"
#import "RJBlurAlertView.h"
#import "UserRatingView.h"
#import "ScrollToMoreView.h"
#import "SortHelper.h"
#import "MyCommunityView.h"
//#import "ChatMessagesTableViewController.h"
#import "ChatPresentation.h"
#import "AuthNavigationViewController.h"
#import "FirstRideView.h"
#import "CPALogger.h"
#import "DirectionService.h"
#import "OtherRequestsModal.h"

#import <ZGNavigationBarTitle/ZGNavigationBarTitleViewController.h>

@interface DriverPassengerViaXibViewController ()
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *personalInfo;
@property (weak, nonatomic) IBOutlet UIView *statisticView;
@property (weak, nonatomic) IBOutlet UIView *aboutMeView;
@property (weak, nonatomic) IBOutlet UIView *myInterestsView;
@property (weak, nonatomic) IBOutlet UIView *myCommunitiesView;
@property (weak, nonatomic) IBOutlet UIView *scrollToViewMoreView;
@property (weak, nonatomic) IBOutlet UIImageView *buttonsView;
@property (weak, nonatomic) IBOutlet CPAButton *declineButton;
@property (weak, nonatomic) IBOutlet CPAButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrowForScrollMoreView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *personalInfoConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *aboutMeConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myInterestConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myCommunitiesConstraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statisticViewConstraintsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lastConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollToMoreConstraintHeight;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@end

@implementation DriverPassengerViaXibViewController {
    PersonalInformationXib *driverPersonalInfo;
    DriverInformation *driverInformationBox;
    AboutMeInformation *aboutMeInfo;
    UserRatingView *ratingView;
    ScrollToMoreView *scrollMoreView;
    MyInterestsView *myInterests;
    MyCommunityView *myCommunities;
    GMSCameraPosition *_camera;
    
    NSMutableDictionary *refusedDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    refusedDic = [NSMutableDictionary new];
    
    [self.navigationItem setBackButtonWithTarget:self.navigationController action:@selector(popViewControllerAnimated:)];
    self.lastConstraint.constant = 0;
    
    if (self.isHiddenBottomBar) {
        [self.mainScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    } else {
        [self.mainScrollView setContentInset:UIEdgeInsetsMake(0, 0, HeightWithPercent(20), 0)];
    }
    
    self.mainScrollView.delegate = self;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setDoesRelativeDateFormatting:YES];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSString *dateString = [dateFormatter stringFromDate:[SortHelper getStringDateWithString:self.userData.date]];
    
    [self setTitle:[NSString stringWithFormat:@"%@", dateString]];
    [self setSubtitle:[NSString stringWithFormat:@"%@ - %@", [NSDate getDateWithString:self.userData.timeStart], [NSDate getDateWithString:self.userData.timeEnd]]];
    [self.view setBackgroundColor:RGBColor(0xffffff, 1)];
    
    _camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(57, 57) zoom:15];
    [_mapView setCamera:_camera];
    
    if (self.isHiddenBottomBar) {
        [self.scrollToViewMoreView setHidden:YES];
        [self.buttonsView setHidden:YES];
        [self.acceptButton setHidden:YES];
        [self.declineButton setHidden:YES];
    } else {
        [self.buttonsView setHidden:NO];
        [self.acceptButton setHidden:NO];
        [self.declineButton setHidden:NO];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateWithNotify) name:@"updateWithNotify" object:nil];
    [self initalizeViews];
}

- (void)viewWillAppear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [super viewWillAppear:animated];
    
    __block RJBlurAlertView *alertView;
    
    [[OnLineResolver sharedInstance] getUserInfoByIdIsLocal:YES userId:self.userData.user_id success:^(GetUserInfoInputModel *response) {
        [self setUserInfo:response];
        [self updateViewsData];
    } failure:^{
        [RJBlurAlertView dissmissGlobalView];
        alertView = [[RJBlurAlertView alloc] initWithTitle:nil text:@"Oooops, try letter" cancelButton:NO];
        [alertView setAnimationType:RJBlurAlertViewAnimationTypeFade];
        
        [alertView show];
    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)updateWithNotify {
    [[OnLineResolver sharedInstance] getUserInfoByIdIsLocal:NO userId:self.userData.user_id success:^(GetUserInfoInputModel *response) {
        [self setUserInfo:response];
        [self.navigationItem setChatNotificationButton:self action:@selector(chatClicked:) messagesCount:self.userInfo.unreadMessages];
        
        UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(callAction:)];
        UIImage *image = [[UIImage imageNamed:@"call-card"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [anotherButton setImage:image];
        [anotherButton setTintColor:RGBColor(0xffffff, 1)];
        
        [self.navigationItem setRightBarButtonItems:@[self.navigationItem.rightBarButtonItem, anotherButton]];
        
        [self updateViewsData];
    } failure:nil];
}

- (void)updateViewsData {
    self.aboutMeConstraintHeight.constant = 0;
    self.statisticViewConstraintsHeight.constant = 0;
    
    [driverPersonalInfo setUserId:self.userData.user_id];
    [driverPersonalInfo setAvatarURL:[NSURL URLWithString:self.userData.pictureURL]];
    [driverPersonalInfo setFirstName:self.userData.name];
    [driverPersonalInfo rideTimeWithType:self.userData.type];
    
    [aboutMeInfo setContent:self.userInfo.about];
    
    [driverInformationBox setRating:[NSString stringWithFormat:@"%0.1f", [self.userInfo.rating floatValue]/10]];
    [driverInformationBox setRides:[NSString stringWithFormat:@"%d", [self.userInfo.rides intValue]]];
    [driverInformationBox setJoined:[self getFormattedInformationDate]];
    [driverInformationBox setCO2Info:[NSString stringWithFormat:@"%dkg", [self.userInfo.co2saved intValue]]];
    
    [myInterests setInterests:self.userInfo.interests];
    DDLogDebug(@"My comunityes: %@", self.userInfo.communities);
    [myCommunities setCommunity:self.userInfo.communities];
    DDLogDebug(@"community count = %lu", (unsigned long)self.userInfo.communities.count);
    float height = self.userInfo.communities.count * HeightWithPercent(14) + 30;
    if (self.userInfo.communities.count > 0){
        [myCommunities setFrame:CGRectMake(0, 0, WidthWithPercent(100), height)];
        self.myCommunitiesConstraintHeight.constant = height;
    }
    else {
        [myCommunities setFrame:CGRectMake(0, 0, WidthWithPercent(100), height)];
        self.myCommunitiesConstraintHeight.constant = 0;
    }
    
    self.aboutMeConstraintHeight.constant = CGRectGetHeight(aboutMeInfo.frame);
    self.statisticViewConstraintsHeight.constant = CGRectGetHeight(driverInformationBox.frame);
}

- (void)initalizeViews {
    self.mapConstraintHeight.constant = HeightWithPercent(52.7);
    CGFloat minusHeight = self.mapConstraintHeight.constant + self.personalInfoConstraintHeight.constant + self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height - 1;
    self.scrollToMoreConstraintHeight.constant = HeightWithPercent(100) - minusHeight;
    
    [self.navigationController.navigationBar setBarTintColor:self.isHiddenBottomBar ? RGBColor(0x1bd19f, 1) : RGBColor(0xff8400, 1)];
    [self.navigationItem setChatNotificationButton:self action:@selector(chatClicked:) messagesCount:self.userInfo.unreadMessages];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:@selector(callAction:)];
    UIImage *image = [[UIImage imageNamed:@"call-card"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [anotherButton setImage:image];
    [anotherButton setTintColor:RGBColor(0xffffff, 1)];
    
    [self.navigationItem setRightBarButtonItems:@[self.navigationItem.rightBarButtonItem, anotherButton]];
    
    NSMutableArray *homeCoordinate = [NSMutableArray arrayWithArray:[self.userData.GPSFrom componentsSeparatedByString:@","]];
    NSMutableArray *workCoordinate = [NSMutableArray arrayWithArray:[self.userData.GPSTo componentsSeparatedByString:@","]];
    NSMutableArray *homeSelfCoordinate = [NSMutableArray arrayWithArray:[[CurrentUser sharedInstance].homeGps componentsSeparatedByString:@","]];
    NSMutableArray *workSelfCoordinate = [NSMutableArray arrayWithArray:[[CurrentUser sharedInstance].workGps componentsSeparatedByString:@","]];
    
    DDLogDebug(@"home self = %@, work self = %@", homeSelfCoordinate, workSelfCoordinate);
    if (homeCoordinate.count > 0 && workCoordinate.count > 0) {
        [self buildMarkerOnMapWithPaxCoordinates:CLLocationCoordinate2DMake([homeCoordinate[0] doubleValue], [homeCoordinate[1] doubleValue])
                                         paxWork:CLLocationCoordinate2DMake([workCoordinate[0] doubleValue], [workCoordinate[1] doubleValue])
                           andSelfHomeCoordinate:CLLocationCoordinate2DMake([homeSelfCoordinate[0] doubleValue], [homeSelfCoordinate[1] doubleValue])
                              selfWorkCoordinate:CLLocationCoordinate2DMake([workSelfCoordinate[0] doubleValue], [workSelfCoordinate[1] doubleValue])];
    } else {
        [self buildMarkerOnMapWithPaxCoordinates:CLLocationCoordinate2DMake(0, 0)
                                         paxWork:CLLocationCoordinate2DMake(0, 0)
                           andSelfHomeCoordinate:CLLocationCoordinate2DMake(0, 0)
                              selfWorkCoordinate:CLLocationCoordinate2DMake(0, 0)];
    }
    
    [self.buttonsView setImage:[UIImage imageNamed:@"bottom_panding_buddy_view_bcg"]];
    
    SVGKImage *crossImage = [SVGKImage imageNamed:@"i_decline_white"];
    [crossImage setSize:CGSizeMake(25, 25)];
    
    [self.declineButton setType:CPAButtonTypeLightGray];
    [self.declineButton setBackgroundColor:RGBColor(0xd0d0d0, 1.0)];
    [self.declineButton addTarget:self action:@selector(declineAction) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *declineImage = [[UIImageView alloc]initWithImage:crossImage.UIImage];
    [declineImage setCenter:CGPointMake(CGRectGetWidth(self.declineButton.frame)/2, CGRectGetHeight(self.declineButton.frame)/2)];
    [self.declineButton addSubview:declineImage];
    
    SVGKImage *checkGreen = [SVGKImage imageNamed:@"i_check_green"];
    [checkGreen setSize:CGSizeMake(32, 25)];
    [self.acceptButton setType:CPAButtonTypeLightGreen];
    [self.acceptButton setBackgroundColor:RGBColor(0xc4f3e6, 1.0)];
    [self.acceptButton addTarget:self action:@selector(acceptAction) forControlEvents:UIControlEventTouchUpInside];
    UIImageView *acceptImage = [[UIImageView alloc]initWithImage:checkGreen.UIImage];
    [acceptImage setCenter:CGPointMake(CGRectGetWidth(self.acceptButton.frame)/2, CGRectGetHeight(self.acceptButton.frame)/2)];
    [self.acceptButton addSubview:acceptImage];
    
    driverPersonalInfo = [[[NSBundle mainBundle] loadNibNamed:@"PersonalInfoNew" owner:self options:nil] objectAtIndex:0];
    [driverPersonalInfo setFrame:CGRectMake(0, 0, WidthWithPercent(100), CGRectGetHeight(self.personalInfo.frame))];

    [driverPersonalInfo.pickUpButton addTarget:self action:@selector(pickUpAction) forControlEvents:UIControlEventTouchUpInside];
    [driverPersonalInfo.dropOffButton addTarget:self action:@selector(dropOffAction) forControlEvents:UIControlEventTouchUpInside];
    [driverPersonalInfo setRating:[self.userData.rating floatValue] / 10];
    [self.personalInfo addSubview:driverPersonalInfo];
    
    driverInformationBox = [[DriverInformation alloc] initWithFrame:CGRectMake(0, 0, WidthWithPercent(100), 97)];
    [self.statisticView addSubview:driverInformationBox];
    
    self.aboutMeConstraintHeight.constant = 0;
    aboutMeInfo = [[AboutMeInformation alloc] initWithFrame:CGRectMake(0, 0, WidthWithPercent(100), 0)];
    
    [self.aboutMeView addSubview:aboutMeInfo];
    
    
    self.myInterestConstraintHeight.constant = 0;
    myInterests = [[MyInterestsView alloc] init];
    [myInterests setDelegate:self];
    [myInterests setFrame:CGRectMake(0, 0, WidthWithPercent(100), 0)];
    [self.myInterestsView addSubview:myInterests];
    
    self.myCommunitiesConstraintHeight.constant = 0;
    myCommunities = [[MyCommunityView alloc]init];
    
    [self.myCommunitiesView addSubview:myCommunities];
}

- (void) buildMarkerOnMapWithPaxCoordinates:(CLLocationCoordinate2D)paxHomeCoordinate
                                    paxWork:(CLLocationCoordinate2D)paxWorkCoordinate
                      andSelfHomeCoordinate:(CLLocationCoordinate2D)selfHomeCoordinate
                         selfWorkCoordinate:(CLLocationCoordinate2D)selfWorkCoordinate {
    _camera = [GMSCameraPosition cameraWithTarget:paxHomeCoordinate zoom:15];
    [_mapView setCamera:_camera];
    
    NSArray *markers = @[
                         [HomeMarker markerWithMap:self.mapView coordinate:paxHomeCoordinate type:self.userData.type visible:YES],
                         [HomeSelfMarker markerWithMap:self.mapView coordinate:selfHomeCoordinate type:self.userData.type visible:YES],
                         [WorkMarker markerWithMap:self.mapView coordinate:paxWorkCoordinate type:self.userData.type visible:YES],
                         [WorkSelfMarker markerWithMap:self.mapView coordinate:selfWorkCoordinate type:self.userData.type visible:YES]
                         ];
    
    [_mapView focusMapToShowAllMarkers:markers];
    [self.mapView sortAndDrowRoutes:self.userData.type markers:markers];
}

- (IBAction)chatClicked:(id)sender {
//    ChatMessagesTableViewController *chat = [[ChatMessagesTableViewController alloc] init];
    ChatPresentation *chat = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"chatRoom"];
    [chat setIsNewChat:YES];
    [chat setUserId:self.userData.user_id];
    
    AuthNavigationViewController *anvc = [[AuthNavigationViewController alloc] initWithRootViewController:chat];
    [self presentViewController:anvc animated:YES completion:nil];
}

- (void)dropOffAction {
    NSString *coord = ([BOOKING_STATUS_MORNING isEqualToString:self.userData.type]) ? self.userData.GPSTo : self.userData.GPSFrom;
    NSMutableArray *homeCoordinate = [NSMutableArray arrayWithArray:[coord componentsSeparatedByString:@","]];
    _camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake([homeCoordinate[0] doubleValue], [homeCoordinate[1] doubleValue]) zoom:14];
    [_mapView animateToCameraPosition:_camera];
}

- (void)pickUpAction {
    NSString *coord = ([BOOKING_STATUS_MORNING isEqualToString:self.userData.type]) ? self.userData.GPSFrom : self.userData.GPSTo;
    NSMutableArray *workCoordinate = [NSMutableArray arrayWithArray:[coord componentsSeparatedByString:@","]];
    _camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake([workCoordinate[0] doubleValue], [workCoordinate[1] doubleValue]) zoom:14];
    [_mapView animateToCameraPosition:_camera];
}

- (IBAction)declineAction {
    BookingDriverInfo *currentDriver = self.userData;
    
    //Is a dictionary empty?
    if ([[CurrentUser sharedInstance] refusedPassangers] == nil) {
        //A dictionary is empty
        [self showModalViewAtDeclineRideWithInfo:currentDriver andSaveResultTo:refusedDic];
        
    } else {
        // A dictionary is not empty
        if ([refusedDic count] < 1)
            refusedDic = [NSMutableDictionary dictionaryWithDictionary:[[CurrentUser sharedInstance] refusedPassangers]];
        
        BOOL isAlredyDecline = NO;
        int value = -1;
        
        for (NSString *key in refusedDic) {
            if ([key isEqualToString:[NSString stringWithFormat:@"%d", currentDriver.user_id]]) {
                //A dictionary already contain user id
                isAlredyDecline = YES;
                value = [refusedDic[key] intValue];
                break;
            }
        }
        
        // value = 1 -- forever decline
        // value = 0 -- only this time decline
        if (isAlredyDecline) {
            if (value)
                [self declineForever:currentDriver];
            else
                [self declineOnlyThisTime:currentDriver];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self showModalViewAtDeclineRideWithInfo:currentDriver andSaveResultTo:refusedDic];
        }
    }
}

- (void)showModalViewAtDeclineRideWithInfo:(BookingDriverInfo *)currentDriver andSaveResultTo:(NSMutableDictionary *)refDictionary{
    OtherRequestsModal *modalContent = [[[NSBundle mainBundle] loadNibNamed:@"OtherRequestsModal" owner:self options:nil] objectAtIndex:0];
    [modalContent.modalContent setText:[NSString stringWithFormat:NSLocalizedString(@"future_requests_text", nil), currentDriver.name, ([BOOKING_STATUS_MORNING isEqualToString:currentDriver.type])?@"work":@"home", currentDriver.name, currentDriver.type]];
    
    RJBlurAlertView *alertView = [[RJBlurAlertView alloc] initWithTitle:nil contentView:modalContent cancelButton:YES];
    [alertView.okButton setTitle:@"Yes" forState:UIControlStateNormal];
    [alertView.cancelButton setTitle:@"No" forState:UIControlStateNormal];
    
    
    [[Mixpanel sharedInstance] track:APP_DRIVER_REQ_DENY];
    [alertView setCompletionBlock:^(RJBlurAlertView *alert, UIButton *button) {
        if (button == alert.okButton) {
            [self declineForever:currentDriver];
            [refDictionary setObject:@(YES) forKey:[NSString stringWithFormat:@"%d", currentDriver.user_id]];
            [[CurrentUser sharedInstance] setRefusedPassangers:refDictionary];
        } else {
            [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
            [refDictionary setObject:@(NO) forKey:[NSString stringWithFormat:@"%d", currentDriver.user_id]];
            [[CurrentUser sharedInstance] setRefusedPassangers:refDictionary];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [alertView show];
}

- (void)declineForever:(BookingDriverInfo *)currentDriver {
    OpertationsModel *model = [[OpertationsModel alloc] init];
    
    model.operations = (id)[NSMutableArray new];
    
    OpertationModel *operation = [[OpertationModel alloc] init];
    [operation setUserId:currentDriver.user_id];
    [operation setOperation:([BOOKING_STATUS_MORNING isEqualToString:currentDriver.type])?DISABLE_MORNING:DISABLE_EVENING];
    [model.operations addObject:operation];
    
    [[OnLineResolver sharedInstance] modyfyBuddy:MODIFY_DRIVER_BUDDY operations:model success:^{
        [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
    } failure:^(NSString *errorMessage) {
        [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
    }];
    
    [[OnLineResolver sharedInstance] modyfyBuddy:MODIFY_DRIVER_BUDDY operations:model success:^{
        [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
    } failure:^(NSString *errorMessage) {
        [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
    }];
}

- (void)declineOnlyThisTime:(BookingDriverInfo *)currentDriver {
    [[OnLineResolver sharedInstance] driverDeclineRideRequest:currentDriver.id];
}

- (void)acceptAction {
    int bookingId = self.userData.id;
    
    if ([self.userData.free_ride boolValue] && [[CurrentUser sharedInstance] corpCommunityId] <= 0) {
        FirstRideView *view = [[[NSBundle mainBundle] loadNibNamed:@"FreeRide" owner:self options:nil] objectAtIndex:0];
        [view setFrame:CGRectMake(0, 0, 235, 120)];
        [view setName:self.userData.name];
        
        RJBlurAlertView *alertView = [[RJBlurAlertView alloc] initWithTitle:nil contentView:view cancelButton:YES];
        [alertView.cancelButton setTitle:@"No" forState:UIControlStateNormal];
        [alertView.okButton setTitle:@"I got it." forState:UIControlStateNormal];
        [alertView setAnimationType:RJBlurAlertViewAnimationTypeFade];
        
        [alertView setCompletionBlock:^(RJBlurAlertView *alert, UIButton *button) {
            if (button == alert.okButton) {
                [[OnLineResolver sharedInstance] driverAcceptRideRequest:bookingId];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        
        [alertView show];
    } else {
        [[OnLineResolver sharedInstance] driverAcceptRideRequest:bookingId];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSString *)getFormattedInformationDate {
    NSDate* date = [NSDate date];
    if ([self.userInfo.regDate intValue] > 0) {
        date = [NSDate dateWithTimeIntervalSince1970:[self.userInfo.regDate doubleValue]];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM. yyyy"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    return dateString;
}

- (void)recalculateMyInterestsFrame:(CGRect)frame {
    self.myInterestConstraintHeight.constant = CGRectGetHeight(frame);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (!self.isHiddenBottomBar) {
        double verticalOffset = fabs(scrollView.contentOffset.y);
        float alpha = (verticalOffset / CGRectGetHeight(self.scrollToViewMoreView.frame) > 1) ? 1 : verticalOffset / CGRectGetHeight(self.scrollToViewMoreView.frame);
        if (scrollView.contentOffset.y >= 0) {
            //DDLogDebug(@"alpha = %f", alpha);
//            [singleView setAlpha:alpha];
            [self.scrollToViewMoreView setAlpha:1-alpha];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    DDLogDebug(@"decelerate =  %d", decelerate);
    if (!decelerate) {
        DDLogDebug(@"in decelerate");
        if (!self.isHiddenBottomBar) {
            [self hideOrDisplayView:scrollView];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    DDLogDebug(@"did end");
    [self hideOrDisplayView:scrollView];
}

- (void) hideOrDisplayView:(UIScrollView *)scrollView {
    double verticalOffset = fabs(scrollView.contentOffset.y);
    float alpha = (verticalOffset / CGRectGetHeight(self.scrollToViewMoreView.frame) > 1) ? 1 : verticalOffset / CGRectGetHeight(self.scrollToViewMoreView.frame);
    if (scrollView.contentOffset.y >= 0) {
        DDLogDebug(@"alpha = %f", alpha);
        if (alpha < 0.5 && alpha != 0.0) {
            [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else if (alpha >= 0.5 && alpha != 1.0) {
            [self.mainScrollView setContentOffset:CGPointMake(0, self.mainScrollView.contentOffset.y + CGRectGetHeight(self.scrollToViewMoreView.frame)/2) animated:YES];
        }
    }
}

- (IBAction)callAction:(id)sender {
    [[Mixpanel sharedInstance] track:APP_CALL];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://+971%@", self.userData.phone]]];
}

@end
