//
//  DirectionService.m
//  carpoolarabia_ios
//
//  Created by Mac on 3/1/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "DirectionService.h"

@implementation DirectionService {
@private
    BOOL _sensor;
    BOOL _alternatives;
    NSURL *_directionsURL;
    NSArray *_waypoints;
}

static NSString *kMDDirectionsURL = @"http://maps.googleapis.com/maps/api/directions/json?";

- (void)setDirectionsQuery:(NSDictionary *)query withSelector:(SEL)selector
              withDelegate:(id)delegate{
    NSArray *waypoints = [query objectForKey:@"waypoints"];
    NSMutableArray *waypointsMutable = [NSMutableArray arrayWithArray:waypoints];
    NSString *origin = [waypointsMutable firstObject];
    NSString *destination = [waypointsMutable lastObject];
    
    NSMutableString *url = [NSMutableString stringWithFormat:@"%@&origin=%@&destination=%@", kMDDirectionsURL, origin, destination];
    
    if(waypointsMutable.count > 0) {
        [url appendString:@"&waypoints=optimize:true"];
        
        for(int i=0; i < waypointsMutable.count; i++){
            [url appendString: @"|"];
            [url appendString:[waypoints objectAtIndex:i]];
        }
    }
    url = [url stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
    _directionsURL = [NSURL URLWithString:url];
    NSLog(@"url = %@", _directionsURL);
    [self retrieveDirections:selector withDelegate:delegate];
}
- (void)retrieveDirections:(SEL)selector withDelegate:(id)delegate {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSData* data = [NSData dataWithContentsOfURL:_directionsURL];
        [self fetchedData:data withSelector:selector withDelegate:delegate];
    });
}

- (void)fetchedData:(NSData *)data withSelector:(SEL)selector withDelegate:(id)delegate{
    NSError* error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    [delegate performSelector:selector withObject:json];
}

@end
