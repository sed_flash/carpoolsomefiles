//
//  PersonalInformationXib.h
//  carpoolarabia_ios
//
//  Created by Admin on 20/02/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserRatingView.h"

@interface PersonalInformationXib : UIView

@property (nonatomic, strong) DriverBuddy *userInfo;
@property (nonatomic, assign) int userId;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pickUpCenterPosition;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dropOffCenterPosition;

@property (strong, nonatomic) IBOutlet UIButton *pickUpButton;
@property (strong, nonatomic) IBOutlet UIButton *dropOffButton;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSURL *avatarURL;
@property (assign, nonatomic) float rating;

- (void)rideTimeWithType:(NSString *)type;

@end
