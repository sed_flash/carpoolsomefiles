//
//  PersonalInformationXib.m
//  carpoolarabia_ios
//
//  Created by Admin on 20/02/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "PersonalInformationXib.h"

@interface PersonalInformationXib ()

@property (strong, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UserRatingView *ratingView;

@end

@implementation PersonalInformationXib {
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_avatar setImage:[UIImage imageWithSVGNamed:@"i_path_profile_grey" targetSize:CGSizeMake(43, 43) fillColor:RGBColor(0x67717D, 1)]];
    [_avatar.layer setCornerRadius:CGRectGetHeight(_avatar.frame)/2];
    [_avatar.layer setMasksToBounds:YES];
    [_avatar.layer setBorderColor:RGBColor(0xffffff, 1).CGColor];
    [_avatar.layer setBorderWidth:2];

    NSMutableAttributedString *pickUpStr = [[NSMutableAttributedString alloc] initWithString:_pickUpButton.titleLabel.text];
    [pickUpStr addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [_pickUpButton.titleLabel.text length])];
    [_pickUpButton setAttributedTitle:pickUpStr forState:UIControlStateNormal];
    
    NSMutableAttributedString *dropOffStr = [[NSMutableAttributedString alloc] initWithString:_dropOffButton.titleLabel.text];
    [dropOffStr addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [_dropOffButton.titleLabel.text length])];
    [_dropOffButton setAttributedTitle:dropOffStr forState:UIControlStateNormal];

    self.pickUpCenterPosition.constant = -WidthWithPercent(36.3);
    self.dropOffCenterPosition.constant = WidthWithPercent(36.3);
}

- (void)rideTimeWithType:(NSString *)type {
    if ([type isEqualToString:BOOKING_STATUS_MORNING]) {
        self.pickUpCenterPosition.constant = -WidthWithPercent(36.3);
        self.dropOffCenterPosition.constant = WidthWithPercent(36.3);
    } else if ([type isEqualToString:BOOKING_STATUS_EVENING]) {
        self.pickUpCenterPosition.constant = WidthWithPercent(36.3);
        self.dropOffCenterPosition.constant = -WidthWithPercent(36.3);
    }
}

- (void)setFirstName:(NSString *)firstName {
    [self.firstNameLabel setText:firstName];
}

- (void)setAvatarURL:(NSURL *)avatarURL {
    [_avatar sd_setImageWithURL:avatarURL placeholderImage:[UIImage imageWithSVGNamed:@"i_path_profile_grey" targetSize:CGSizeMake(43, 43) fillColor:RGBColor(0x67717D, 1)]];
}

- (void)setRating:(float)rating {
    [self.ratingView setRating:rating];
}

@end
