//
//  DirectionService.h
//  carpoolarabia_ios
//
//  Created by Mac on 3/1/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DirectionService : NSObject
- (void)setDirectionsQuery:(NSDictionary *)object withSelector:(SEL)selector
              withDelegate:(id)delegate;
- (void)retrieveDirections:(SEL)sel withDelegate:(id)delegate;
- (void)fetchedData:(NSData *)data withSelector:(SEL)selector
       withDelegate:(id)delegate;
@end
