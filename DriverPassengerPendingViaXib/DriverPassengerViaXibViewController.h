//
//  DriverPassengerViaXibViewController.h
//  carpoolarabia_ios
//
//  Created by Admin on 19/02/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyInterestsView.h"

@interface DriverPassengerViaXibViewController : UIViewController <UIScrollViewDelegate, UserInformationDelegate, RecalculateDelegate>
@property (nonatomic, strong) BookingDriverInfo *userData;
@property (nonatomic, strong) GetUserInfoInputModel *userInfo;


@property (nonatomic, assign) BOOL isHiddenBottomBar;
@end
