//
//  LocationHelper.m
//  carpoolarabia_ios
//
//  Created by Mac on 2/11/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "LocationHelper.h"
#import "BackgroundHelper.h"
#import "CPALogger.h"

@implementation LocationHelper{
    CLLocationManager *locationManager;
    NSString *latestCoordinates;
    NSTimer *timer;
}

+ (LocationHelper *)sharedInstance {
    static LocationHelper *lh;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        lh = [[LocationHelper alloc] init];
    });
    return lh;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        locationManager = [CLLocationManager new];
        [locationManager setDelegate:self];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationEnterBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(stopUpdateLocationOnBackground)
                                                     name:@"stopUpdateLocationOnBackground"
                                                   object:nil];
    }
    
    return self;
}

- (void) applicationEnterBackground {
    DDLogDebug(@"Application enter background");
    [locationManager stopUpdatingLocation];
    [locationManager startUpdatingLocation];
    [timer invalidate];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                             target:self
                                           selector:@selector(uploadCoordirate)
                                           userInfo:nil
                                            repeats:YES];
    [[BackgroundHelper sharedInstance] startBackgroundMode];
}

- (void)stopUpdateLocationOnBackground {
    DDLogDebug(@"Canceled background update location");
    [[BackgroundHelper sharedInstance] endBackgroundTask];
}

- (void)startUpdateLocation {
    if (![CLLocationManager locationServicesEnabled]){
        DDLogDebug(@"location services are disabled");
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        DDLogDebug(@"location services are blocked by the user");
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized){
        DDLogDebug(@"location services are enabled");
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
        DDLogDebug(@"about to show a dialog requesting permission");
    }
    
    if ([CLLocationManager locationServicesEnabled]) {
        DDLogDebug(@"enabled");
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8) {
            [locationManager requestAlwaysAuthorization];
        }
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
            locationManager.allowsBackgroundLocationUpdates = YES;
        }
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
            
            BOOL yes = YES;
            
            NSMethodSignature* signature = [[CLLocationManager class] instanceMethodSignatureForSelector: @selector( setAllowsBackgroundLocationUpdates: )];
            NSInvocation* invocation = [NSInvocation invocationWithMethodSignature: signature];
            [invocation setTarget: locationManager];
            [invocation setSelector: @selector( setAllowsBackgroundLocationUpdates:) ];
            [invocation setArgument: &yes atIndex: 2];
            [invocation invoke];
        }
        [locationManager startUpdatingLocation];
        [timer invalidate];
        timer = [NSTimer scheduledTimerWithTimeInterval:5.0f
                                                 target:self
                                               selector:@selector(uploadCoordirate)
                                               userInfo:nil
                                                repeats:YES];
    } else {
        DDLogDebug(@"disabled");
    }
}

- (void) setAllowsBackgroundLocationUpdates:(NSMethodSignature *)signature {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9) {
        locationManager.allowsBackgroundLocationUpdates = YES;
    }
}

- (void)stopUpdateLocation {
    DDLogDebug(@"Stop update location");
    [timer invalidate];
    timer = nil;
    [locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (locations.count > 0) {
        CLLocation *latestCoordinate = [locations lastObject];
        latestCoordinates = [NSString stringWithFormat:@"%f, %f", latestCoordinate.coordinate.latitude, latestCoordinate.coordinate.longitude];
        DDLogDebug(@"latest coordinates = %@", latestCoordinates);
    }
}

- (void) uploadCoordirate {
    if (latestCoordinates == nil) DDLogDebug(@"PUSTAYA STROKA");
    else{
    DDLogDebug(@"upload coordinate %@ to server", latestCoordinates);
    [[OnLineResolver sharedInstance] sendLocationsForActioveRoutes:latestCoordinates];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
