//
//  BackgroundHelper.m
//  carpoolarabia_ios
//
//  Created by Mac on 2/11/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "BackgroundHelper.h"
#import "CPALogger.h"
#import "AppDelegate.h"

@implementation BackgroundHelper {
    __block UIBackgroundTaskIdentifier bgTask;
    UIApplication* application;
}

+ (BackgroundHelper *)sharedInstance {
    static BackgroundHelper *bh;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        bh = [[BackgroundHelper alloc] init];
    });
    
    return bh;
}

- (UIBackgroundTaskIdentifier)startBackgroundMode {
    application = [UIApplication sharedApplication];
    bgTask = UIBackgroundTaskInvalid;
    [application endBackgroundTask:bgTask];
    DDLogDebug(@"New background task");
    if([application respondsToSelector:@selector(beginBackgroundTaskWithExpirationHandler:)]) {
        bgTask = [application beginBackgroundTaskWithExpirationHandler:^{
            DDLogDebug(@"The BG task will be stopped");
            [application endBackgroundTask:bgTask];
            bgTask = UIBackgroundTaskInvalid;
        }];
    }

    return bgTask;
}

- (void)endBackgroundTask {
    [application endBackgroundTask:bgTask];
    bgTask = UIBackgroundTaskInvalid;
}

@end
