//
//  BackgroundHelper.h
//  carpoolarabia_ios
//
//  Created by Mac on 2/11/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BackgroundHelper : NSObject

+ (BackgroundHelper*)sharedInstance;

- (UIBackgroundTaskIdentifier)startBackgroundMode;

- (void)endBackgroundTask;

@end
