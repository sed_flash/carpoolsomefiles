//
//  LocationHelper.h
//  carpoolarabia_ios
//
//  Created by Mac on 2/11/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationHelper : NSObject <CLLocationManagerDelegate>
+ (LocationHelper *)sharedInstance;
- (void)startUpdateLocation;
- (void)stopUpdateLocation;

@end
