//
//  ChatPresentation.m
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "ChatPresentation.h"
#import "ChatTableView.h"
#import "ChatMessagesModel.h"
#import "CPALogger.h"

int const kMessageEnterFieldHeight = 33;
int const kSumTopAndBottonPadding = 12;
NSString *const placeholderText = @"Write a message";

@interface ChatPresentation () <ChatDataSource, ChatRoomsProtocol, MessageProtocol, UITextViewDelegate>

@property (strong, nonatomic) IBOutlet ChatTableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *messageEnterView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIImageView *sendMessageImage;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *keyboardHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageEnterFieldHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeightConstraint;

@end

@implementation ChatPresentation {
    float lastHeight;
    ChatMessagesModel *chatModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lastHeight = 0;
    chatModel = [[ChatMessagesModel alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateCurrentChatThroughNotification) name:@"updateCurrentChat" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wasUpdateDates:) name:@"UpdateChatDatesNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertRows:) name:@"insertNotification" object:nil];
    [[OnLineResolver sharedInstance] setDelegate:self];
    
    self.view.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;
    _tableView.messageProtocol = self;
    _messageEnterView.delegate = self;
    
    SVGKImage *sendImage = [SVGKImage imageNamed:@"i_chat_send"];
    [sendImage setSize:_sendMessageImage.frame.size];
    [self.sendMessageImage setImage:sendImage.UIImage];
    
    [_messageEnterView.layer setCornerRadius:4.0];
    [_messageEnterView.layer setMasksToBounds:YES];
    [_messageEnterView.layer setBorderWidth:1.0];
    [_messageEnterView.layer setBorderColor:RGBColor(0xd1d1d5, 1).CGColor];
    _messageEnterView.text = placeholderText;
    _messageEnterView.textColor = [UIColor lightGrayColor];
    
    if (self.isNewChat) {
        [self.navigationItem setBackButtonWithTarget:self action:@selector(closeModal)];
        
        [[OnLineResolver sharedInstance] createChatRoom:self.userId success:^{
            [[OnLineResolver sharedInstance] getOneChatRoom:self.userId success:^(ChatRoom *chatRoom) {
                [self setTitle:[NSString stringWithFormat:NSLocalizedString(@"chat_with", nil), chatRoom.name]];
                [self setAvatarUrl:[NSURL URLWithString:chatRoom.avatar]];
                [self setUserName:chatRoom.name];
                [self updateCurrentChat:NO];
            } failure:^{
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
        } failure:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    } else {
        [self setTitle:[NSString stringWithFormat:NSLocalizedString(@"chat_with", nil), self.userName]];
        [self.navigationItem setBackButtonWithTarget:self.navigationController action:@selector(popViewControllerAnimated:)];
    }
}

- (void)myKeyboardWillShow :(NSNotification *)notification {
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    _keyboardHeightConstraint.constant = CGRectGetHeight(keyboardRect);
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self tableViewContentOffset];
                     }];
}

- (void)myKeyboardWillHide :(NSNotification *)notification {
    _keyboardHeightConstraint.constant = 0;
    if ([_messageEnterView.text isEqualToString:@""]) {
        _messageEnterView.text = placeholderText;
        _messageEnterView.textColor = [UIColor lightGrayColor]; //optional
    }
    [_messageEnterView resignFirstResponder];
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)closeModal {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIView methods

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateCurrentChat:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myKeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myKeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [_tableView scrollToBottomTableView];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [UIView animateWithDuration:0.05 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        CGRect frame;
        frame = _messageEnterView.frame;
        frame.size.height = self.messageEnterView.contentSize.height;
        if (_messageEnterView.contentSize.height <= 67){
            [_messageEnterView setFrame:frame];
        }
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.tableView = nil;
}

#pragma mark - ChatRooms Protocol

- (void)updateCurrentChatThroughNotification {
    [self updateCurrentChat:YES];
}

- (void)updateCurrentChat:(BOOL)isNewMsg {
    // If notification is equal "updateCurrentChat"
    if (isNewMsg) {
        NSDate *currentDate = [NSDate date];
        NSTimeInterval timestamp = [currentDate timeIntervalSince1970];
        chatModel.isNeedReplaceArray = YES;
        [chatModel getMessagesByUserId:self.userId withOldDate:[NSString stringWithFormat:@"%f", timestamp] isNew:YES];
    } else
        [chatModel getMessagesByUserId:self.userId withOldDate:nil isNew:NO];
}

- (void)wasUpdateDates:(NSNotification *)notification {
    BOOL isNew = [notification.object boolValue];
    [_tableView setIsNewMessage:isNew];
    [_tableView reloadData];
}

- (void)wasUpdateDates {
    [_tableView setIsNewMessage:YES];
    [_tableView reloadData];
}

- (void)insertRows:(NSNotification *)notification {
    int rows = [notification.object intValue];
    [_tableView insertRows:rows];
}

- (void)newMessages {
    [chatModel getNewMessage];
}

#pragma mark - TableView DataSource

- (NSInteger)numberOfRowForSection:(ChatTableView *)tableView {
    return [chatModel itemsCount];
}

- (Message *)tableView:(ChatTableView *)tableView messageAtIndex:(NSInteger)index {
    return [chatModel getMessageAtRow:index];
}

#pragma mark - Message protocol

- (int)getOwnerIdForRow:(NSInteger)row {
    return [chatModel getMessageAtRow:row].owner_id;
}

- (NSURL *)getAvatarUrl {
    return self.avatarUrl;
}

- (void)updateChatWithTimeStamp:(NSString *)timestamp {
    if (!chatModel.isEmpty)
        [chatModel getMessagesByUserId:self.userId withOldDate:timestamp isNew:NO];
}

- (void)setIsBegin:(BOOL)isBegin {
    [chatModel setIsBegin:isBegin];
}

- (BOOL)isBegin {
    return [chatModel isBegin];
}

- (void)needToAddData:(BOOL)isNeedAdd {
    chatModel.needToAddData = YES;
}

#pragma mark - UITextView Delegate

- (void)textViewDidChange:(UITextView *)textView {
    [self resizeBottomView:textView forDefault:NO];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:placeholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (IBAction)sendMessage:(id)sender {
    
    if ([[[_messageEnterView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] stringByTrimmingCharactersInSet:
          [NSCharacterSet whitespaceCharacterSet]] length] > 0 &&
        ![_messageEnterView.text isEqualToString:placeholderText]) {
        NSString *uniText = [NSString stringWithUTF8String:[_messageEnterView.text UTF8String]];
        NSData *msgData = [uniText dataUsingEncoding:NSNonLossyASCIIStringEncoding];
        NSString *goodMsg = [[NSString alloc] initWithData:msgData encoding:NSUTF8StringEncoding];
        
        Message *newMessage = [[Message alloc] init];
        [newMessage setMessage:goodMsg];
        [newMessage setDate:[NSDate date]];
        [newMessage setOwner_id:[[CurrentUser sharedInstance] userId]];
        
        [chatModel addMessage:newMessage];
        [self wasUpdateDates];
        
        SendChatMessageOutputModel *sendMessageOutputModel = [[SendChatMessageOutputModel alloc] init];
        sendMessageOutputModel.sendUserId = self.userId;
        sendMessageOutputModel.token = [[CurrentUser sharedInstance] token];
        sendMessageOutputModel.message = goodMsg;
        
        
        [[OnLineResolver sharedInstance] sendChatMessage:sendMessageOutputModel userId:self.userId];
        [_messageEnterView setText:@""];
        [self resizeBottomView:_messageEnterView forDefault:YES];
    }
}

- (void)resizeBottomView:(UITextView *)textView forDefault:(BOOL)isDefault {
    
    CGFloat contentSizeHeight = (!isDefault) ? self.messageEnterView.contentSize.height : kMessageEnterFieldHeight;
    
    if (contentSizeHeight > 67) {
        contentSizeHeight = 67;
    }
    
    if (lastHeight != contentSizeHeight) {
        self.bottomViewHeightConstraint.constant = contentSizeHeight + kSumTopAndBottonPadding;
        [self.sendMessageImage setHidden:contentSizeHeight <= kMessageEnterFieldHeight];
        [self tableViewContentOffset];
        lastHeight = contentSizeHeight;
    }
}

- (void)tableViewContentOffset {
    [self.view layoutIfNeeded];
    CGPoint offset = CGPointMake(0, self.tableView.tableView.contentSize.height - self.tableView.tableView.frame.size.height);
    [self.tableView.tableView setContentOffset:offset];
}

@end
