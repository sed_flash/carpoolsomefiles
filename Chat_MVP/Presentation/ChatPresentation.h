//
//  ChatPresentation.h
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatPresentation : UIViewController

@property (nonatomic, assign) int userId;
@property (nonatomic, strong) NSURL *avatarUrl;
@property (nonatomic, strong) NSString *userName;

@property (nonatomic, assign) BOOL isNewChat;

@end
