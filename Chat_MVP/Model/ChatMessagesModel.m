//
//  ChatMessagesModel.m
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "ChatMessagesModel.h"

@implementation ChatMessagesModel{
    NSMutableArray *tableData;
    BOOL isNewMessage;
}

//+ (ChatMessagesModel *)sharedInstance {
//    static ChatMessagesModel *instance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        instance = [[ChatMessagesModel alloc] init];
//    });
//    
//    return instance;
//}

- (instancetype)init {
    self = [super init];
    if (self) {
        tableData = [NSMutableArray new];
    }
    
    return self;
}

- (void)getMessagesByUserId :(int)userId withOldDate: (NSString *)timestamp isNew :(BOOL)isNew_ {
    isNewMessage = isNew_;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[OnLineResolver sharedInstance] getChatMessages:userId oldDate:timestamp nextDate:nil];
    });
}

- (void)getNewMessage {
    
    if (_isNeedReplaceArray) {
        // This condition for situations when user get or send (check read) new message
        
        NSMutableArray *tempArr = [[[OnLineResolver sharedInstance] currentRoomMessages] messages];
        
        Message *msg2 = tempArr[tempArr.count - 1];
        
        if ([[CurrentUser sharedInstance] userId] != msg2.owner_id)
//            [self detectUnaddedMessages:tempArr];
            tableData = [[[OnLineResolver sharedInstance] currentRoomMessages] messages];
        else
            [self detectUnreadStatus:tempArr];
        msg2 = nil;
        
    } else if (tableData.count > 0 && _needToAddData) {
        // This condition for situation when user is scrlling view to up
        
        NSMutableArray *arr = [[[OnLineResolver sharedInstance] currentRoomMessages] messages];
        
        if (arr.count == 0) {
            _isEmpty = YES;
            return;
        }
        tableData = [[arr arrayByAddingObjectsFromArray:tableData] mutableCopy];
        _isNeedReplaceArray = NO;
        
        // Commented this three lines if need to update whole table
        // instead of insert new rows in table
        NSInteger rowsForInsert = arr.count;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"insertNotification" object:@(rowsForInsert)];
        _needToAddData = NO;
        return;
    } else {
        // In beginning
        
        tableData = [[[OnLineResolver sharedInstance] currentRoomMessages] messages];
    }
    _isNeedReplaceArray = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateChatDatesNotification" object:@(isNewMessage)];
}

- (void)detectUnreadStatus:(NSMutableArray *)tempArr {
    
    @try {
        int index;
        for (int i = 1; ; i++) {
            Message *temp = tableData[tableData.count - i];
            if (temp.status) {
                index = i - 1;
                break;
            }
        }
        while (index) {
            [tableData replaceObjectAtIndex:tableData.count - index withObject:tempArr[tempArr.count - index]];
            index--;
        }
    } @catch (NSException *exception) {
        NSLog(@"In exception block!!!");
    }
    
}

//- (void)detectUnaddedMessages:(NSMutableArray *)tempArr {
//    int index;
//    for (int i = 1; ; i++) {
//        Message *temp1 = tableData[tableData.count - 1];
//        Message *temp2 = tempArr[tempArr.count - i];
//        if (temp1.message_id == temp2.message_id) {
//            index = i - 1;
//            break;
//        }
//    }
//    while (index) {
//        [tableData addObject:tempArr[tempArr.count - index]];
//        index--;
//    }
//}

- (NSInteger)itemsCount {
    return [tableData count];
}

- (Message *)getMessageAtRow :(NSInteger)row {
    return [tableData objectAtIndex:row];
}

- (void)addMessage:(Message *)message {
    [tableData addObject:message];
}

@end
