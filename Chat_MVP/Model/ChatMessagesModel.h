//
//  ChatMessagesModel.h
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatMessagesModel : NSObject

@property (nonatomic, assign, readonly) NSInteger itemsCount;
@property (nonatomic, assign) BOOL isNeedReplaceArray;
@property (nonatomic, assign) BOOL isEmpty;
@property (nonatomic, assign) BOOL isBegin;
@property (nonatomic, assign) BOOL needToAddData;

//+ (ChatMessagesModel *)sharedInstance;
- (void)getMessagesByUserId :(int)userId withOldDate: (NSString *)timestamp isNew :(BOOL)isNew_;
- (void)getNewMessage;
- (Message *)getMessageAtRow :(NSInteger)row;
- (void)addMessage:(Message *)message;

@end
