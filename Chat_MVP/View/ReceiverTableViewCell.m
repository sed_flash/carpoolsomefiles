//
//  ReceiverTableViewCell.m
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "ReceiverTableViewCell.h"

@interface ReceiverTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIImageView *messageTongueImage;
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *hasReadImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageFieldWidthConstraint;

@end

@implementation ReceiverTableViewCell {
    SVGKImage *svgImg;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
    UIImage *ballon = [self.messageTongueImage.image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 21, 20, 27)];
    [self.messageTongueImage setImage:ballon];
    
    [self.messageLabel setNumberOfLines:MAXFLOAT];
    [self.messageLabel.layer setZPosition:MAXFLOAT];
    self.messageFieldWidthConstraint.constant = WidthWithPercent(60);
    
    _avatar.image = [UIImage imageNamed:@"user-placeholder"];
    [_avatar.layer setCornerRadius:CGRectGetHeight(_avatar.frame)/2];
    [_avatar.layer setMasksToBounds:YES];
    [_avatar sd_setImageWithURL:[NSURL URLWithString:[[CurrentUser sharedInstance] avatarUrl]] placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:_avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setMessage:(Message *)message {
    const char *jsonString = [message.message UTF8String];
    NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
    NSString *goodMsg = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];

    [self.messageLabel setText:[goodMsg stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}

- (float)heightOfCell {
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    return CGRectGetHeight(self.messageTongueImage.frame) + CGRectGetHeight(self.sendTimeLabel.frame) + 15;
}

- (void)setSenderTime:(NSString *)senderTime {
    [self.sendTimeLabel setText:senderTime];
}

- (void)setHasRead:(BOOL)hasRead {
    if (hasRead) {
        //прочитано - две галочки
        
        svgImg = [SVGKImage imageNamed:@"chat_check_double"];
    } else {
        //одна галочка
        
        svgImg = [SVGKImage imageNamed:@"chat_check_single"];
    }
    
    [svgImg setSize:_hasReadImageView.frame.size];
    [self.hasReadImageView setHidden:NO];
    [_hasReadImageView setImage:[svgImg changeBackgroundColorWithColor:RGBColor(0x576c8d, 1)]];
}

- (void)setIsHiddenImage:(BOOL)isHiddenImage {
    [self.hasReadImageView setHidden:isHiddenImage];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
