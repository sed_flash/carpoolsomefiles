//
//  ChatTableView.h
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChatTableView;

@protocol ChatDataSource <NSObject>

@required
- (NSInteger)numberOfRowForSection:(ChatTableView *)tableView;
- (Message *)tableView:(ChatTableView *)tableView messageAtIndex:(NSInteger)index;

@end

@protocol MessageProtocol <NSObject>

@required
- (int)getOwnerIdForRow:(NSInteger)row;
- (NSURL *)getAvatarUrl;
- (void)updateChatWithTimeStamp:(NSString *)timestamp;
- (void)setIsBegin:(BOOL)isBegin;
- (BOOL)isBegin;
- (void)needToAddData:(BOOL)isNeedAdd;

@end

@interface ChatTableView : UIView

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id <ChatDataSource>dataSource;
@property (weak, nonatomic) id <MessageProtocol>messageProtocol;
@property (assign, nonatomic) BOOL isNewMessage;

- (void)reloadData;
- (void)scrollToBottomTableView;
- (void)insertRows:(int)rows;

@end
