//
//  ChatTableView.m
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "ChatTableView.h"
#import "SenderTableViewCell.h"
#import "ReceiverTableViewCell.h"

#import "CPALogger.h"

#define kReceiver @"receiverId"
#define kSender @"senderId"

@interface ChatTableView () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ChatTableView {
    BOOL isBegin_, first;
    float oldContentSize;
    NSInteger currentRow;
    NSInteger oldRowsCount;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [_messageProtocol setIsBegin:NO];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    isBegin_ = YES;
    first = YES;
    
    [_tableView registerNib:[UINib nibWithNibName:@"SenderTableViewCell"   bundle:nil] forCellReuseIdentifier:kSender];
    [_tableView registerNib:[UINib nibWithNibName:@"ReceiverTableViewCell" bundle:nil] forCellReuseIdentifier:kReceiver];
}

#pragma mark - Table View Data Source

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row < 5 && [_messageProtocol isBegin]) {
        [_messageProtocol setIsBegin:NO];
        [_messageProtocol needToAddData:YES];
        oldRowsCount = [self.dataSource numberOfRowForSection:self];
        NSDate *date = [self.dataSource tableView:self messageAtIndex:indexPath.row].date;
        NSTimeInterval timestamp = [date timeIntervalSince1970];
        NSString *str = [NSString stringWithFormat:@"%f", timestamp];
        
        currentRow = indexPath.row;
        [_messageProtocol updateChatWithTimeStamp:str];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource numberOfRowForSection:self];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id cell_;
    
    if ([[CurrentUser sharedInstance] userId] != [self.messageProtocol getOwnerIdForRow:indexPath.row]) {
        SenderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kSender];
        
        cell.message = [self.dataSource tableView:self messageAtIndex:indexPath.row];
        [cell setAvatarUrl:[self.messageProtocol getAvatarUrl]];
        
        if (indexPath.row > 0) {
            Message *oldMessage = [self.dataSource tableView:self messageAtIndex:indexPath.row - 1];
            
            if (!([self dateVisibleWithDates:[self.dataSource tableView:self messageAtIndex:indexPath.row].date secondDate:oldMessage.date] && [self.messageProtocol getOwnerIdForRow:indexPath.row] == oldMessage.owner_id))
            {
                cell.senderTime = [[self dateStringWithDate:[self.dataSource tableView:self messageAtIndex:indexPath.row].date] uppercaseString];
                
            } else {
                cell.senderTime = @"";
            }
        }
        
        [tableView setRowHeight:cell.heightOfCell];
        cell_ = cell;
    } else {
        ReceiverTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReceiver];
        cell.message = [self.dataSource tableView:self messageAtIndex:indexPath.row];
        
        if (indexPath.row > 0) {
            Message *oldMessage = [self.dataSource tableView:self messageAtIndex:indexPath.row - 1];
            
            if (!([self dateVisibleWithDates:[self.dataSource tableView:self messageAtIndex:indexPath.row].date secondDate:oldMessage.date] && [self.messageProtocol getOwnerIdForRow:indexPath.row] == oldMessage.owner_id))
            {
                cell.senderTime = [[self dateStringWithDate:[self.dataSource tableView:self messageAtIndex:indexPath.row].date] uppercaseString];
                [cell setHasRead:[self.dataSource tableView:self messageAtIndex:indexPath.row].status];
            
            } else {
                cell.senderTime = @"";
                [cell setIsHiddenImage:YES];
            }
        }
        
        [tableView setRowHeight:cell.heightOfCell];
        cell_ = cell;
    }
    
    return cell_;
}

#pragma mark - Additional methods

- (NSString *)dateStringWithDate :(NSDate *)date {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"d MMM yyyy HH:mm"];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSString *firstMinutes = [df stringFromDate:date];
    
    return firstMinutes;
}

- (BOOL)dateVisibleWithDates :(NSDate *)firstDate secondDate:(NSDate *)secondDate {
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"d MMM yyyy HH:mm"];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSString *firstMinutes = [df stringFromDate:firstDate];
    NSString *secondMinutes = [df stringFromDate:secondDate];
    
    return ([firstMinutes isEqualToString:secondMinutes]);
}

#pragma mark - UITableView reboot and offset

- (void)reloadData {
    [UIView animateWithDuration:0 animations:^{
        oldContentSize = _tableView.contentSize.height;
        [self.tableView reloadData];
    } completion:^(BOOL finished) {
        [_messageProtocol setIsBegin:YES];
        [self addTableViewOffset];
    }];
}

- (void)insertRows:(int)rows {
//    NSMutableArray *insertRows = [NSMutableArray new];
//    for (int i = 0; i < rows; i++) {
//        [insertRows addObject:[NSIndexPath indexPathForRow:i inSection:0]];
//    }
    
    [UIView animateWithDuration:0 animations:^{
        dispatch_async(dispatch_get_main_queue(), ^{
//            [_tableView beginUpdates];
//            [_tableView insertRowsAtIndexPaths:insertRows withRowAnimation:UITableViewRowAnimationAutomatic];
//            [_tableView endUpdates];
            [_tableView reloadData];
        });
    } completion:^(BOOL finished) {
        NSInteger idxNew = [self.dataSource numberOfRowForSection:self];
        NSInteger idxOld = oldRowsCount;
        
        if ((idxNew - idxOld + currentRow) < 0) {
            DDLogDebug(@"LESS THEN ZERO!!!!!");
        }
        NSInteger indexScroll = (idxNew - idxOld + currentRow) < 0 ? 0 : idxNew - idxOld + currentRow;
        if (indexScroll > [_tableView numberOfRowsInSection:0]) {
            DDLogDebug(@"GATHER THEN LAST INDEX!!!");
            indexScroll = 0;
        }
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:indexScroll
                                              inSection:0];

        DDLogDebug(@"number of row in section = %ld, index.row = %ld", (long)[_tableView numberOfRowsInSection:0], (long)index.row);
        [_tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:NO];

        [_messageProtocol setIsBegin:YES];
    }];
}

- (void)addTableViewOffset {
    if ([self.dataSource numberOfRowForSection:self] > 0) {
        if (self.tableView.contentSize.height >= self.tableView.frame.size.height) {
            [self layoutIfNeeded];
            
            CGPoint offset;
//            if (_isNewMessage || first) {
                first = NO;
                offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
                [self.tableView setContentOffset:offset];
//            }
            
            // Uncommented this lines if need to insert rows
            // instead of update all table data
//            else {
//                    NSInteger idxNew = [self.dataSource numberOfRowForSection:self];
//                    NSInteger idxOld = oldRowsCount;
//                    
//                    NSIndexPath *index = [NSIndexPath indexPathForRow:idxNew - idxOld + currentRow
//                                                            inSection:0];
//                    [_tableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:NO];
//                    [_messageProtocol setIsBegin:YES];
//                
//            }
            
        }
    }
}

- (void)scrollToBottomTableView {
    if (self.tableView.contentSize.height >= self.tableView.frame.size.height) {
        [self layoutIfNeeded];
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
