//
//  SenderTableViewCell.m
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import "SenderTableViewCell.h"

@interface SenderTableViewCell ()

@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIImageView *messageTongueImage;
@property (weak, nonatomic) IBOutlet UILabel *sendTimeLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageFieldWidthConstraint;

@end

@implementation SenderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIImage *ballon = [self.messageTongueImage.image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 27, 20, 21)];
    [self.messageTongueImage setImage:ballon];
    
    [self.messageLabel setNumberOfLines:MAXFLOAT];
    [self.messageLabel.layer setZPosition:MAXFLOAT];
    self.messageFieldWidthConstraint.constant = WidthWithPercent(60);
    
    _avatar.image = [UIImage imageNamed:@"user-placeholder"];
    [_avatar.layer setCornerRadius:CGRectGetHeight(_avatar.frame)/2];
    [_avatar.layer setMasksToBounds:YES];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setMessage:(Message *)message {
    const char *jsonString = [message.message UTF8String];
    NSData *jsonData = [NSData dataWithBytes:jsonString length:strlen(jsonString)];
    NSString *goodMsg = [[NSString alloc] initWithData:jsonData encoding:NSNonLossyASCIIStringEncoding];
    
    [self.messageLabel setText:[goodMsg stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
}

- (void)setAvatarUrl:(NSURL *)avatarUrl {
    _avatarUrl = avatarUrl;
    [self.avatar sd_setImageWithURL:avatarUrl placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:self.avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
}

- (float)heightOfCell {
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    NSLog(@"date label height = %f", CGRectGetHeight(self.sendTimeLabel.frame));
    return CGRectGetHeight(self.messageTongueImage.frame) + CGRectGetHeight(self.sendTimeLabel.frame) + 15;
}

- (void)setSenderTime:(NSString *)senderTime {
    [self.sendTimeLabel setText:senderTime];
}

@end
