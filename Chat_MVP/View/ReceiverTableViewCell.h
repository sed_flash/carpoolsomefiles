//
//  ReceiverTableViewCell.h
//  carpoolarabia_ios
//
//  Created by Mac on 4/12/16.
//  Copyright © 2016 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiverTableViewCell : UITableViewCell

@property (nonatomic, copy) Message *message;
@property (nonatomic, copy) NSString *senderTime;
@property (nonatomic, assign, readonly) float heightOfCell;
@property (nonatomic, assign) BOOL hasRead;
@property (nonatomic, assign) BOOL isHiddenImage;

@end
