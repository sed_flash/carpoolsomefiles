//
//  DriverBuddyListXibTableViewCell.m
//  carpoolarabia_ios
//
//  Created by Mac on 11/19/15.
//  Copyright © 2015 gritsay. All rights reserved.
//

#import "DriverBuddyListXibTableViewCell.h"

@interface DriverBuddyListXibTableViewCell ()
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (strong, nonatomic) IBOutlet UIImageView *notification;
@property (strong, nonatomic) IBOutlet UILabel *notificationCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *firstName;
@property (strong, nonatomic) IBOutlet UIImageView *home;
@property (strong, nonatomic) IBOutlet UIImageView *work;
@property (strong, nonatomic) IBOutlet UIImageView *oneDirection;
@property (strong, nonatomic) IBOutlet UIImageView *bothDirections;
@property (strong, nonatomic) IBOutlet UIView *nonActiveView;
@end

@implementation DriverBuddyListXibTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [self.avatar.layer setCornerRadius:CGRectGetHeight(self.avatar.frame)/2];
    [self.avatar.layer setMasksToBounds:YES];
    
    [self.notification.layer setCornerRadius:CGRectGetHeight(self.notification.frame)/2];
    [self.notification.layer setMasksToBounds:YES];
    
    [self svgImages];
}

- (void) addBothDirectionsToSuperView {
    [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridework_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.bothDirections setImage:[UIImage imageWithSVGNamed:@"i_ridehome_green" targetSize:self.bothDirections.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.bothDirections setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.mainView addSubview:self.bothDirections];
    NSDictionary *views = @{@"both" : self.bothDirections, @"one" : self.oneDirection, @"work" : self.work, @"home" : self.home};
    [self.mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[home]-8-[both]-8-[work]" options:0 metrics:nil views:views]];
    [self.mainView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-26-[both]-1-[one]" options:0 metrics:nil views:views]];
}

- (void) drawingRidesWithToHome:(BOOL)toHome andToWork:(BOOL)toWork{
    [self.home setHidden:NO];
    [self.work setHidden:NO];
    [self.oneDirection setHidden:NO];
    if (self.bothDirections != nil) [self.bothDirections setHidden:NO];
    [self.avatar setAlpha:1.0f];
    if(toHome && toWork){
        self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.work setTintColor:RGBColor(0x28DA93, 1)];
        self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.home setTintColor:RGBColor(0x28DA93, 1)];
        [self addBothDirectionsToSuperView];
    } else if(toHome && !toWork){
        [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridehome_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
        self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.work setTintColor:RGBColor(0xe9e9e9, 1)];
        self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.home setTintColor:RGBColor(0x28DA93, 1)];
        [self.bothDirections removeFromSuperview];
    } else if(!toHome && toWork){
        [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridework_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
        self.work.image = [self.work.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.work setTintColor:RGBColor(0x28DA93, 1)];
        self.home.image = [self.home.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.home setTintColor:RGBColor(0xe9e9e9, 1)];
        [self.bothDirections removeFromSuperview];
    } else if(!toHome && !toWork){
        [self.home setHidden:YES];
        [self.work setHidden:YES];
        [self.oneDirection setHidden:YES];
        [self.bothDirections setHidden:YES];
        [self.avatar setAlpha:0.5f];
    }
}

- (void)svgImages {
    SVGKImage *avatarImage = [SVGKImage imageNamed:@"i_portrait_grey"];
    [avatarImage setSize:self.avatar.frame.size];
    [self.avatar setImage:avatarImage.UIImage];
    
    [self.home setImage:[UIImage imageWithSVGNamed:@"i_home_green" targetSize:self.home.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.work setImage:[UIImage imageWithSVGNamed:@"i_work_green" targetSize:self.work.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.oneDirection setImage:[UIImage imageWithSVGNamed:@"i_ridework_green" targetSize:self.oneDirection.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.bothDirections setImage:[UIImage imageWithSVGNamed:@"i_ridehome_green" targetSize:self.bothDirections.frame.size fillColor:RGBColor(0x28DA93, 1)]];
    [self.arrowImage setImage:[UIImage imageWithSVGNamed:@"i_ios_rightbtn" targetSize:CGSizeMake(8, 16) fillColor:RGBColor(0x67717D, 1)]];
}

- (void)drawingRides {
    [self drawingRidesWithToHome:_toHome andToWork:_toWork];
}

- (void)setName:(NSString *)name {
    [self.firstName setText:name];
}

- (void)setNotificationCount:(int)notificationCount {
    if (notificationCount == 0) {
        [self.notification setHidden:YES];
        [self.notificationCountLabel setHidden:YES];
    } else {
        [self.notification setHidden:NO];
        [self.notificationCountLabel setHidden:NO];
        [self.notificationCountLabel setText:[NSString stringWithFormat:@"%d", notificationCount]];
    }
}

- (void)setAvatarURL:(NSURL *)avatarURL {
    [self.avatar sd_setImageWithURL:avatarURL placeholderImage:[UIImage imageWithSVGNamed:@"i_portrait_grey" targetSize:self.avatar.frame.size fillColor:RGBColor(0x67717C, 1)]];
}

- (UIImage *)blackAndWhiteImage:(UIImage *)image
{
    CIImage *beginImage = [CIImage imageWithCGImage:image.CGImage];
    
    CIImage *output = [CIFilter filterWithName:@"CIPhotoEffectTonal" keysAndValues:kCIInputImageKey, beginImage, nil].outputImage;
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgiimage = [context createCGImage:output fromRect:output.extent];
    UIImage *newImage = [UIImage imageWithCGImage:cgiimage];
    
    CGImageRelease(cgiimage);
    
    return newImage;
}

@end
