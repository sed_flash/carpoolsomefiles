//
//  DriverBuddyListXibTableViewCell.h
//  carpoolarabia_ios
//
//  Created by Mac on 11/19/15.
//  Copyright © 2015 gritsay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriverBuddyListXibTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImage;
@property (assign, nonatomic) BOOL toHome;
@property (assign, nonatomic) BOOL toWork;
@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) int notificationCount;
@property (strong, nonatomic) NSURL *avatarURL;

@property (nonatomic, strong) DriverBuddy *userData;

- (void)drawingRides;
@end
